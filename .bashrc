#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

force_color_prompt=yes

powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BASH_SELECT=1
. /usr/share/powerline/bindings/bash/powerline.sh

alias ls='ls --color=always'
alias list='lsd -lAh --color=always'
alias update='pikaur -Syyyuu --noedit'
alias purge_cache='pikaur -Scc'
alias remove_orphans='pikaur -Qtdq | pikaur -Rscnu -'
alias nano='nano -LSlmq'

neofetch