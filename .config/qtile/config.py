#==[NOTES FROM THE AUTHOR]==#


# THIS CONFIGURATION OF QTILE IS INTENDED FOR ARCH LINUX USERS.
# PEOPLE USING OTHER DISTROS ARE WELCOME TO TEST IT. HOWEVER, REVIEW THE CODE BEFORE UTILIZING IT.


#==[IMPORTS]==#


import os, subprocess, libqtile

from typing import List  # noqa: F401

from libqtile import qtile, bar, layout, widget, hook, backend
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal


#==[GENERAL VARIABLES]==#


terminal = guess_terminal()
python_shell = terminal + " --class 'Python Shell','Python Shell' -t 'Python Shell' -e python -q"
system_monitor = terminal + ' --class "Terminal","Terminal" -t "BTop" -e btop'

home = os.path.expanduser('~')
app_scripts = home + "/.config/archish-gah/app-scripts/"
system_scripts = home + "/.config/archish-gah/system-scripts/"

city_id = '793138'
open_weather = 'brave --app=https://openweathermap.org/city/' + city_id
available_updates = app_scripts + 'available-updates.sh'

#--rofi
rofi_drun = 'rofi -show drun -theme ~/.config/archish-gah/rofi-themes/ag-drun.rasi -steal-focus'
rofi_window = 'rofi -show window -theme ~/.config/archish-gah/rofi-themes/ag-window.rasi'
rofi_windowcd = 'rofi -show windowcd -theme ~/.config/archish-gah/rofi-themes/ag-window.rasi'
rofi_powermenu = system_scripts + "powermenu.sh"
rofi_unified_search = app_scripts + "unified-search.sh"
rofi_script_runner = system_scripts + "script-runner.sh"
rofi_calendar = system_scripts + "calendar.sh"

#--keys

mod = 'mod4'
alt = 'mod1'
ctrl = 'control'

numpad = [
		"KP_Insert",	# KEY PAD 0
		"KP_End",		# KEY PAD 1
		"KP_Down",		# KEY PAD 2
		"KP_Next",		# KEY PAD 3
		"KP_Left",		# KEY PAD 4
		"KP_Begin",		# KEY PAD 5
		"KP_Right",		# KEY PAD 6
		"KP_Home",		# KEY PAD 7
		"KP_Up",		# KEY PAD 8
		"KP_Prior",		# KEY PAD 9
		]


#==[CUSTOM FUNCTIONS]==#


def cycle_workspaces(direction, move_window):
	def _inner(qtile):
		current = qtile.groups.index(qtile.current_group)
		destination = (current + direction) % len(groups)
		if move_window:
			qtile.current_window.togroup(qtile.groups[destination].name)
		qtile.groups[destination].cmd_toscreen()
	return _inner

to_next_group = lazy.function(cycle_workspaces(direction=1, move_window=True))
to_previous_group = lazy.function(cycle_workspaces(direction=-1, move_window=True))
	

#==[LAZY FUNCTIONS]==#


@lazy.function
def float_to_front(qtile):
	for group in qtile.groups:
		for window in group.windows:
			if window.floating:
				window.cmd_bring_to_front()

@lazy.function
def toggle_minimized(qtile):
	for group in qtile.groups:
		for window in group.windows:
			if window.minimized:
				window.cmd_toggle_minimize()

@lazy.function
def minimize(qtile):
	for group in qtile.groups:
		for window in group.windows:
			if not window.minimized:
				window.cmd_toggle_minimize()


#==[KEYBOARD AND MOUSE SHORTCUTS]==#


keys = [
    # Switch between windows
	Key([mod], "Left",
		lazy.layout.left(),
		desc="Move focus to left"),
	Key([mod], "Right",
		lazy.layout.right(),
		desc="Move focus to right"),
	Key([mod], "Down",
		lazy.layout.down(),
		desc="Move focus down"),
	Key([mod], "Up",
		lazy.layout.up(),
		desc="Move focus up"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
	Key([mod, alt], "Left",
		lazy.layout.shuffle_left(),
		desc="Move window to the left"),
	Key([mod, alt], "Right",
		lazy.layout.shuffle_right(),
		desc="Move window to the right"),
	Key([mod, alt], "Down",
		lazy.layout.shuffle_down(),
		desc="Move window down"),
	Key([mod, alt], "Up",
		lazy.layout.shuffle_up(),
		desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
	Key([mod, ctrl], "Left",
		lazy.layout.grow_left(),
		desc="Grow window to the left"),
	Key([mod, ctrl], "Right",
		lazy.layout.grow_right(),
		desc="Grow window to the right"),
	Key([mod, ctrl], "Down",
		lazy.layout.grow_down(),
		desc="Grow window down"),
	Key([mod, ctrl], "Up",
		lazy.layout.grow_up(),
		desc="Grow window up"),

	# Manage windows.
	Key([mod], "r",
		lazy.layout.normalize(),
		desc="Reset all window sizes"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed per stack, can behave like Max layout, but still with multiple stack panes
	Key([mod], "v",
		lazy.layout.toggle_split(),
		desc="Toggle between split and unsplit sides of stack"),

	# Restart or shutdown Qtile.
	Key([mod, ctrl], "r",
		lazy.restart(),
		desc="Restart Qtile"),
	Key([mod, ctrl], "q",
		lazy.spawn(rofi_powermenu),
		desc="Run the rofi logout menu."),

    # Keys for toggling floating mode, fullscreen and exiting windows.
	Key([mod], "q",
		lazy.window.kill(),
		desc="Kill focused window"),
	Key([mod], "w",
		lazy.window.toggle_floating(),
		desc="Toggle window floating mode"),
	Key([mod], "f",
		lazy.window.toggle_fullscreen(),
		desc="Toggle window fullscreen mode"),

	# Brings floating and minimized windows to the front.
	Key([mod], "t",
		float_to_front,
		desc="Brings floating windows to front."),
	Key([mod], "e",
		toggle_minimized,
		desc="Brings minimized windows to front."),
	Key([mod], "d",
		minimize,
		desc="Minimize all windows."),

    # Move to the left/right group with Ctrl + Alt + [Left/Right].
	Key([alt, ctrl], "Right",
		lazy.screen.next_group(),
		desc="Moves to the right group."),
	Key([alt, ctrl], "Left",
		lazy.screen.prev_group(),
		desc="Moves to the left group."),

	 # Move window to the left/right group with Shift + Mod + [Left/Right].
	Key([mod, "shift"], "Right",
		to_next_group,
		desc="Moves window to the right group."),
	Key([mod, "shift"], "Left",
		to_previous_group,
		desc="Moves window to the left group."),

	# Switches groups between the current and the last visited one using Ctrl + Alt + Backspace.
	Key([mod], "b",
		lazy.screen.toggle_group(),
		desc="Switches groups between the current and the last visited."),

	# Toggle screen focus.
	Key([mod], "c",
		lazy.next_screen(),
		desc="Switches focus to the right screen."),
	Key([mod], "x",
		lazy.prev_screen(),
		desc="Switches focus to the left screen."),

	# Toggle window focus.
	Key([mod], "a",
		lazy.group.next_window(),
		desc="Switches focus to the next window."),
	Key([mod], "z",
		lazy.group.prev_window(),
		desc="Switches focus to the previous window."),

	# Application shortcuts.
	Key([mod], "Return",
		lazy.spawn(terminal),
		desc="Launches the terminal"),
	Key([mod], "p",
		lazy.spawn(python_shell),
		desc="Launches the python shell in the default terminal"),

	# Rofi menus and apps.
	Key([mod], "space",
		lazy.spawn(rofi_drun),
		desc="Spawns the rofi application menu"),
	Key([mod], "Tab",
		lazy.spawn(rofi_windowcd),
		desc="Spawns the rofi windowcd switcher"),
	Key([alt], "Tab",
		lazy.spawn(rofi_window),
		desc="Spawns the rofi window switcher"),
	Key([mod], "s",
		lazy.spawn(rofi_script_runner),
		desc="Spawns the rofi script runner"),

    # Run btop task manager.
	Key([alt, ctrl], "Delete",
		lazy.spawn(system_monitor),
		desc="Spawns the btop terminal system monitor"),

	# Special media keys.
	Key([], "XF86AudioRaiseVolume",
		lazy.spawn(system_scripts + "vol-up.sh"),
		desc="Raises the volume using the keyboard special keys."),
	Key([], "XF86AudioLowerVolume",
		lazy.spawn(system_scripts + "vol-down.sh"),
		desc="Lowers the volume using the keyboard special keys."),
	Key([], "XF86AudioMute",
		lazy.spawn(system_scripts + "mute-toggle.sh"),
		desc="Mutes/Unmutes the volume using the keyboard special keys."),

	# Screenshots using maim.
	Key([], "Print",
		lazy.spawn(system_scripts + "maim-screen.sh"),
		desc="Take a screenshot of the whole screen."),
	Key([ctrl], "Print",
		lazy.spawn(system_scripts + "maim-area.sh"),
		desc="Take a screenshot of the selected area."),
	Key([alt], "Print",
		lazy.spawn(system_scripts + "maim-window.sh"),
		desc="Take a screenshot of the active window."),

	# Keyboard layout switch key.
	Key([alt], "space",
		lazy.widget["keyboardlayout"].next_keyboard(),
		desc="Next keyboard layout."),
]

mouse = [
    Drag([mod], "Button1",
		lazy.window.set_position_floating(),
		start=lazy.window.get_position()),
    Drag([mod], "Button3",
		lazy.window.set_size_floating(),
		start=lazy.window.get_size()),
    Click([mod], "Button2",
		lazy.window.kill(),
		),
]


#==[GROUPS]==#


group_names = [
			("[1]"),
			("[2]"),
			("[3]"),
			("[4]"),
			("[5]"),
			("[6]"),
			("[7]"),
			("[8]"),
			("[9]"),
			]

groups = [Group(name) for name in group_names]

for i, (name) in enumerate(group_names):
	
	keys.append(Key([mod], str(i+1), lazy.group[name].toscreen()))
	keys.append(Key([mod, ctrl], str(i+1), lazy.window.togroup(name)))
	
	keys.append(Key([mod], numpad[i+1], lazy.group[name].toscreen()))
	keys.append(Key([mod, ctrl], numpad[i+1], lazy.window.togroup(name)))


#==[LAYOUTS]==#


layout_theme = {
				"border_width": 2,
                "margin": 2,
                "border_focus": "#5294E2",
                "border_normal": "#AAAAAA",
                }

layouts = [
	layout.Columns(
		**layout_theme,
		border_focus_stack = '#5294E2',
		border_normal_stack = '#AAAAAA',
		border_on_single = True,
		insert_position = 1,
		split = False,
	),
]

# Run the utility of `xprop` to see the wm class and name of an X client.
floating_layout = layout.Floating(
		**layout_theme,
		float_rules=[
			*layout.Floating.default_float_rules,
			Match(wm_class='confirmreset'),  # gitk
			Match(wm_class='makebranch'),  # gitk
			Match(wm_class='maketag'),  # gitk
			Match(wm_class='ssh-askpass'),  # ssh-askpass
			Match(title='branchdialog'),  # gitk
			Match(title='pinentry'),  # GPG key password entry
    
		# My additions to programs launched in floating layout mode.
			Match(title='YAD'),
			Match(wm_class=[
						'arandr',
						'pavucontrol',
						'grsync',
						'lxappearance',
						'gufw.py',
						'xarchiver',
						'lightdm-gtk-greeter-settings',
						'libfm-pref-apps',
						'nm-connection-editor',
						'gsmartcontrol',
						'psensor',
						'usbimager',
						'woeusbgui',
						'rofi',
						'qalculate-gtk',
						'libfm-pref-apps',
						'syncthing-gtk',
						]),
					])


#==[SCREENS]==#


widget_defaults = dict(
    font = 'Jetbrains Mono Regular',
    fontsize = 16,
    padding = 3,
	)

extension_defaults = widget_defaults.copy()

def init_widgets_list():
    widgets_list = [
				# INDEX 0
				widget.Spacer(
					length = 5,
					mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(rofi_drun)},
					),
				# INDEX 1
				widget.Image(
					filename = "~/.config/archish-gah/icons/custom/archish-gah.svg",
					scale = "False",
					mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(rofi_drun)},
					margin = 2,
				),
				# INDEX 2
				widget.Spacer(
					length = 5,
					mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(rofi_drun)},
				),
				# INDEX 3
				widget.GroupBox(
					active = '#EEEEEE',
					inactive = '#6E6E6E',
					block_highlight_text_color = '#EEEEEE',
					disable_drag = True,
					highlight_color = ['#2E2E2E', '#5294E2'],
					highlight_method = 'line',
					other_screen_border = '#AAAAAA',
					other_current_screen_border = '#AAAAAA',
					this_screen_border = '#5294E2',
					this_current_screen_border = '#5294E2',
					urgent_border = '#FF4D4D',
				),
				# INDEX 4
				widget.TaskList(
					max_title_width = 230,
					fontsize = 15,
					title_width_method = 'uniform',
					highlight_method = 'block',
					rounded = False,
					border = '#5294E2',
					spacing = 7,
					padding_x = 10,
					foreground = '#EEEEEE',
					unfocused_border = '#AAAAAA',
					urgent_border = '#FF4D4D',
					txt_minimized = '[Min] ',
					txt_maximized = '[Max] ',
					txt_floating = '[Flt] ',
				),
				# INDEX 5
				widget.Systray(
					icon_size = 24,
					padding = 7,
				),
				# INDEX 6
				widget.KeyboardLayout(
					padding = 7,
					configured_keyboards = ['us', 'rs latin', 'rs'],
					display_map = {'us':'[us_EN]', 'rs latin':'[la_RS]', 'rs':'[cy_RS]'}
				),
				# INDEX 7
				widget.CheckUpdates(
					colour_have_updates = '#FF4D4D',
					colour_no_updates = '#EEEEEE',
					custom_command = 'pikaur -Quq',
					display_format = '[pkg: {updates}]',
					no_update_string = '[pkg: 0]',
					mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(available_updates)},
				),
				# INDEX 8
				widget.OpenWeather(
					cityid = city_id,
					format = '[{temp}°{units_temperature}]',
					mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(open_weather)},
				),
				# INDEX 9
				widget.Clock(
					format = '[%Y.%m.%d. %I:%M:%S %p]',
					mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(rofi_calendar)},
				),
				# INDEX 10
				widget.Spacer(
					length = 5,
					mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(rofi_powermenu)},
				),
				# INDEX 11
				widget.Image(
					filename = "~/.config/archish-gah/icons/custom/exit.svg",
					scale = "False",
					mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(rofi_powermenu)},
					margin = 2,
				),
				# INDEX 12
				widget.Spacer(
					length = 5,
					mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(rofi_powermenu)},
					),
              ]
    return widgets_list

def init_widgets_primary_screen():
	widgets_primary_screen = init_widgets_list()
	return widgets_primary_screen

def init_widgets_secondary_screens():
	widgets_secondary_screens = init_widgets_list()
	del widgets_secondary_screens[5:9]	# Slicing removes unwanted widgets (systray, keyboard layout) on Monitors which are not primary.
	return widgets_secondary_screens
	
def init_screens():
	monitor_number = os.popen("xrandr | grep ' connected ' | awk '{ print $1 }' | wc -l").read().replace("\n", "")
	if monitor_number == "1":
		return [Screen(top = bar.Bar(widgets = init_widgets_primary_screen(), size = 30, background = "#2E2E2E", foreground = "#EEEEEE", opacity = 1,)),]
	elif monitor_number == "2":
		return [Screen(top = bar.Bar(widgets = init_widgets_primary_screen(), size = 30, background = "#2E2E2E", foreground = "#EEEEEE", opacity = 1,)),
				Screen(top = bar.Bar(widgets = init_widgets_secondary_screens(), size = 30, background = "#2E2E2E", foreground = "#EEEEEE", opacity = 1,)),]
	else:
		return [Screen(top = bar.Bar(widgets = init_widgets_primary_screen(), size = 30, background = "#2E2E2E", foreground = "#EEEEEE", opacity = 1,)),
				Screen(top = bar.Bar(widgets = init_widgets_secondary_screens(), size = 30, background = "#2E2E2E", foreground = "#EEEEEE", opacity = 1,)),
				Screen(top = bar.Bar(widgets = init_widgets_secondary_screens(), size = 30, background = "#2E2E2E", foreground = "#EEEEEE", opacity = 1,)),]

if __name__ in ["config", "__main__"]:
	screens = init_screens()
	widgets_list = init_widgets_list()
	widgets_primary_screen = init_widgets_primary_screen()
	widgets_secondary_screens = init_widgets_secondary_screens()


#==[HOOKS]==#


@hook.subscribe.startup_once
def autostart():
	subprocess.Popen([system_scripts + 'autostart-qtile.sh'])

@hook.subscribe.client_new
def floating_dialogs(window):
	dialog = window.window.get_wm_type() == 'dialog'
	transient = window.window.get_wm_transient_for()
	if dialog or transient:
		window.floating = True

@hook.subscribe.client_new
def float_steam(window):
	wm_class = window.window.get_wm_class()
	w_name = window.window.get_name()
	if (
		wm_class == ["Steam", "Steam"]
		and (
			w_name != "Steam"
			or "PMaxSize" in window.window.get_wm_normal_hints().get("flags", ())
		)
	):
		window.floating = True


#==[DEFAULT SETTINGS]==#


auto_fullscreen = True
auto_minimize = True
bring_front_click = True
cursor_warp = False
follow_mouse_focus = False
focus_on_window_activation = "smart"
reconfigure_screens = True
dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
wmname = "LG3D"