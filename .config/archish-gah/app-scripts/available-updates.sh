#!/bin/bash

pkexec pikaur -Syyy

pkgs=$(pikaur -Quq)

updates_theme="$HOME/.config/archish-gah/rofi-themes/ag-list"
no_update_theme="$HOME/.config/archish-gah/rofi-themes/ag-message"
prompt="Search packages:"

if [[ -z $pkgs ]]; then
	pkg_lines=0
else
	pkg_lines=$(echo -e "$pkgs" | wc -l)
fi

if [[ ! -z $pkgs && $pkg_lines -eq 1 ]]; then
	mesg_text="There is $pkg_lines package available for update."
else
	mesg_text="There are $pkg_lines packages available for update."
fi

if [[ -z $pkgs ]]; then
	echo -e "$pkgs" | rofi -e "$mesg_text" -theme $no_update_theme
elif [[ ! -z $pkgs && $pkg_lines -gt 13 ]]; then
	echo -e "$pkgs" | rofi -dmenu -i -mesg "$mesg_text" -p "$prompt" -theme $updates_theme -lines 13
else
	echo -e "$pkgs" | rofi -dmenu -i -mesg "$mesg_text" -p "$prompt" -theme $updates_theme -lines $pkg_lines
fi