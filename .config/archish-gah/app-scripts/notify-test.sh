#!/bin/bash

icon="$HOME/.config/archish-gah/icons/custom/archish-gah.svg"

notify-send -i $icon -u low "[ LOW ]      - NOTIFICATION TEST"
notify-send -i $icon -u normal "[ NORMAL ]   - NOTIFICATION TEST"
notify-send -i $icon -u critical "[ CRITICAL ] - NOTIFICATION TEST"