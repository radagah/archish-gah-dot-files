#!/bin/bash

editor="geany"

rofi_theme="$HOME/.config/archish-gah/rofi-themes/ag-list"

file_prompt="Name your file:"
file=$(rofi -dmenu -p "$file_prompt" -no-fixed-num-lines -no-steal-focus -theme $rofi_theme)

sh='.sh'
html='.html'
css='.css'
py='.py'

extensions=($sh $html $css $py)
array_length=${#extensions[@]}
mod_arr_length=$((${#extensions[@]}-1))

for item in ${extensions[@]}; do
	if [[ $item == ${extensions[$mod_arr_length]} ]]; then
		ex_string+=$item
	else
		ex_string+=$item"\n"
	fi
done

file_exists="File with that name already exists."

function check() {
	if [[ -f "$HOME/Documents/$file$1" ]]; then
		rofi -e "$file_exists"
	elif [[ $file == "" ]]; then
		local file="script_$(date +%Y-%m-%d-%T)"
		touch "$HOME/Documents/$file$1"
		case $1 in
			$sh)
				echo -e "#!/bin/bash" > "$HOME/Documents/$file$1";;
			$py)
				echo -e "#!/usr/bin/python" > "$HOME/Documents/$file$1";;
		esac
		chmod +x "$HOME/Documents/$file$1"
		$editor "$HOME/Documents/$file$1"
		
	else
		touch "$HOME/Documents/$file$1"
		case $1 in
			$sh)
				echo -e "#!/bin/bash" > "$HOME/Documents/$file$1";;
			$py)
				echo -e "#!/usr/bin/python" > "$HOME/Documents/$file$1";;
		esac
		chmod +x "$HOME/Documents/$file$1"
		$editor "$HOME/Documents/$file$1"
	fi
}

choice_prompt="Choose your file extension:"
choice=$(echo -e "$ex_string" | rofi -dmenu -p "$choice_prompt" -l $array_length -theme $rofi_theme)

case $choice in
	$sh)
		check $sh;;
	$html)
		check $html;;
	$css)
		check $css;;
	$py)
		check $py;;
esac