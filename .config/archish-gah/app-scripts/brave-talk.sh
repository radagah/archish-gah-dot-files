#!/bin/bash

rofi_theme="$HOME/.config/archish-gah/rofi-themes/ag-list"
general_prompt="Select Brave Talk option:"
brave_talk_link_prompt="Enter your Brave Talk link:"

brave_talk_new="1. Brave Talk instance - NEW"
brave_talk_link="2. Brave Talk instance - LINK"

options="$brave_talk_new\n$brave_talk_link"

brave_choice=$(echo -e $options | rofi -dmenu -i -p "$general_prompt" -l 2 -theme $rofi_theme)

case $brave_choice in
	$brave_talk_new)
		brave --user-data-dir="$HOME/.cache/brave-talk" --app="https://talk.brave.com/";;
	$brave_talk_link)
		link=$(rofi -dmenu -p "$brave_talk_link_prompt" -no-fixed-num-lines -no-steal-focus -theme $rofi_theme | sed 's/\https:\/\/talk.brave.com\///g')
		brave --user-data-dir="$HOME/.cache/brave-talk" --app="https://talk.brave.com/$link";;
esac