#!/bin/bash

pkgs=$(pikaur -Qeq)

pkg_lines=$(echo -e "$pkgs" | wc -l)

mesg_text="There are $pkg_lines packages installed."
prompt="Search packages:"

rofi_theme="$HOME/.config/archish-gah/rofi-themes/ag-list"

if [[ $pkg_lines -gt 13 ]]; then
	echo -e "$pkgs" | rofi -dmenu -i -mesg "$mesg_text" -p "$prompt" -theme $rofi_theme -lines 13
else
	echo -e "$pkgs" | rofi -dmenu -i -mesg "$mesg_text" -p "$prompt" -theme $rofi_theme -lines $pkg_lines
fi

# TODO
# Merge certain packages into their parent package.