#!/bin/bash

browser="brave"

cat_command="$(cat $HOME/.config/archish-gah/conf-files/unified-search-conf)"
search_engines=$(echo -e "$cat_command" | awk 'NR % 2 == 1' )
search_urls=$(echo -e "$cat_command" | awk 'NR % 2 == 0' )
search_arr_length=$(echo -e "$search_engines" | wc -l)

rofi_theme="$HOME/.config/archish-gah/rofi-themes/ag-list"

if [[ $search_arr_length -gt 13 ]]; then
	rofi_command="rofi -dmenu -i -theme $rofi_theme -l 13"
else
	rofi_command="rofi -dmenu -i -theme $rofi_theme -l $search_arr_length"
fi

num_arr=()
for (( i = 0; i < $search_arr_length; i++ )); do
	num_arr[$i]=$(($i+1))
done

search_engines_arr=()
count=0
for engine in $search_engines; do
	search_engines_arr[$count]=$engine
	count=$(( $count+1 ))
done

search_urls_arr=()
count=0
for url in $search_urls; do
	search_urls_arr[$count]=$url
	count=$(( $count+1 ))
done

function create_cases() {
	
	local dollar='$'
	local q="query"
	local open='('
	local rofi_command='rofi -dmenu -i'
	local sed=" | sed 's/ /+/g'"
	local close=')'
	
	echo "#!/bin/bash"
	echo "case $(echo '$choice') in"
	
	for num in ${num_arr[@]}; do
		local rofi_args=" -p '${search_engines_arr[$(( $num-1 ))]}:' -no-fixed-num-lines -no-steal-focus -theme $rofi_theme"
		echo "	${search_engines_arr[$(( $num-1 ))]}$close"
		echo "		$q=$dollar$open$rofi_command$rofi_args$sed$close"
		echo "		if [[ $dollar$q == '' ]]; then"
		echo "			$browser $(echo "${search_urls_arr[$(( $num-1 ))]}" | awk -F '/' '{ print $1"//"$2$3"/" }')"
		echo "		else"
		echo "			eval '$browser ${search_urls_arr[$(( $num-1 ))]}'"
		echo "		fi;;"
	done
	
	echo "esac"
}

create_cases > $HOME/.config/archish-gah/conf-files/case-statements.sh
chmod +x $HOME/.config/archish-gah/conf-files/case-statements.sh

prompt_msg="Choose search engine"
choice=$(echo -e "$search_engines" | $rofi_command -p "$prompt_msg:")

. $HOME/.config/archish-gah/conf-files/case-statements.sh