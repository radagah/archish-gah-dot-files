#!/bin/bash

function copy_icons() {
	local papirus_ag_dest="/usr/share/icons/Papirus-AG"
	local papirus_ag_source="$1/.config/archish-gah/icons/Papirus-AG-source"
	local folders=$(ls -Ah "$papirus_ag_source")
	
	for folder in $folders; do
		if [[ "$folder" != "index.theme" ]]; then
			subfolders=$(ls -Ah "$papirus_ag_source/$folder" | sed 's/ /\n/g')
			for subfolder in $subfolders; do
				icons=$(ls -Ah "$papirus_ag_source/$folder/$subfolder" | sed 's/ /\n/g')
				for icon in $icons; do
					if [[ ! -d "$papirus_ag_dest/$folder/$subfolder" ]]; then
						mkdir -p "$papirus_ag_dest/$folder/$subfolder"
					fi
					cp "$papirus_ag_source/$folder/$subfolder/$icon" "$papirus_ag_dest/$folder/$subfolder/$icon"
				done
			done
		else
			cp "$papirus_ag_source/index.theme" "$papirus_ag_dest/index.theme"
		fi
	done
}

func=$(declare -f copy_icons)

function update_Papirus_AG() {
	
	local user_home="$HOME"
	
	local confirm_mesg_theme="$HOME/.config/archish-gah/rofi-themes/ag-confirm-mesg"
	local message_theme="$HOME/.config/archish-gah/rofi-themes/ag-message"
	
	local remove_dir="rm -r /usr/share/icons/Papirus-AG"
	local copy_Papirus_Dark="cp -r /usr/share/icons/Papirus-Dark /usr/share/icons/Papirus-AG"
	
	local changed_mesg="Changes finished."
	local unchanged_mesg="No changes were made."
	local question_mesg="Do you want to update the Papirus-AG icons?"
	
	local current_user=$HOME
	
	if [[ ! -d "/usr/share/icons/Papirus-AG" ]]; then
		pkexec $copy_Papirus_Dark
		pkexec bash -c "$func; copy_icons '$user_home'"
		rofi -e "$changed_mesg" -theme $message_theme
	elif [[ -d "/usr/share/icons/Papirus-AG" ]]; then
		choice=$(echo -en "No\nYes" | rofi -dmenu -i -mesg "$question_mesg" -selected-row 0 -l 2 -theme $confirm_mesg_theme)
		case $choice in
			"No")
				rofi -e "$unchanged_mesg" -theme $message_theme;;
			"Yes")
				pkexec $remove_dir
				pkexec $copy_Papirus_Dark
				pkexec bash -c "$func; copy_icons '$user_home'"
				rofi -e "$changed_mesg" -theme $message_theme;;
		esac
	fi
}

update_Papirus_AG