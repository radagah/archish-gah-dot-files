#!/bin/bash
case $choice in
	Brave)
		query=$(rofi -dmenu -i -p 'Brave:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://search.brave.com/
		else
			eval 'brave https://search.brave.com/search?q=$query'
		fi;;
	SearX)
		query=$(rofi -dmenu -i -p 'SearX:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://searx.info/
		else
			eval 'brave https://searx.info/search?q=$query'
		fi;;
	Mojeek)
		query=$(rofi -dmenu -i -p 'Mojeek:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://www.mojeek.com/
		else
			eval 'brave https://www.mojeek.com/search?q=$query'
		fi;;
	DuckDuckGo)
		query=$(rofi -dmenu -i -p 'DuckDuckGo:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://duckduckgo.com/
		else
			eval 'brave https://duckduckgo.com/?q=$query&t=brave'
		fi;;
	Startpage)
		query=$(rofi -dmenu -i -p 'Startpage:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://www.startpage.com/
		else
			eval 'brave https://www.startpage.com/do/search?q=$query'
		fi;;
	Swisscow)
		query=$(rofi -dmenu -i -p 'Swisscow:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://swisscows.com/
		else
			eval 'brave https://swisscows.com/web?query=$query'
		fi;;
	Qwant)
		query=$(rofi -dmenu -i -p 'Qwant:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://www.qwant.com/
		else
			eval 'brave https://www.qwant.com/?q=$query'
		fi;;
	Ecosia)
		query=$(rofi -dmenu -i -p 'Ecosia:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://www.ecosia.org/
		else
			eval 'brave https://www.ecosia.org/search?q=$query'
		fi;;
	Wiby)
		query=$(rofi -dmenu -i -p 'Wiby:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://wiby.me/
		else
			eval 'brave https://wiby.me/?q=$query'
		fi;;
	OpenStreet-Map)
		query=$(rofi -dmenu -i -p 'OpenStreet-Map:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://www.openstreetmap.org/
		else
			eval 'brave https://www.openstreetmap.org/search?query=$query'
		fi;;
	Everipedia)
		query=$(rofi -dmenu -i -p 'Everipedia:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://everipedia.org/
		else
			eval 'brave https://everipedia.org/search?input=$query'
		fi;;
	Wikipedia)
		query=$(rofi -dmenu -i -p 'Wikipedia:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://en.wikipedia.org/
		else
			eval 'brave https://en.wikipedia.org/w/index.php?search=$query'
		fi;;
	Britannica)
		query=$(rofi -dmenu -i -p 'Britannica:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://www.britannica.com/
		else
			eval 'brave https://www.britannica.com/search?query=$query'
		fi;;
	Investopedia)
		query=$(rofi -dmenu -i -p 'Investopedia:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://www.investopedia.com/
		else
			eval 'brave https://www.investopedia.com/search?q=$query'
		fi;;
	OneLook-Dictonary)
		query=$(rofi -dmenu -i -p 'OneLook-Dictonary:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://www.onelook.com/
		else
			eval 'brave https://www.onelook.com/?w=$query'
		fi;;
	OneLook-Thesaurus)
		query=$(rofi -dmenu -i -p 'OneLook-Thesaurus:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://www.onelook.com/
		else
			eval 'brave https://www.onelook.com/thesaurus/?s=$query'
		fi;;
	Collins-Dictionary)
		query=$(rofi -dmenu -i -p 'Collins-Dictionary:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://www.collinsdictionary.com/
		else
			eval 'brave https://www.collinsdictionary.com/dictionary/english/$query'
		fi;;
	Free-Dictionary)
		query=$(rofi -dmenu -i -p 'Free-Dictionary:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://www.thefreedictionary.com/
		else
			eval 'brave https://www.thefreedictionary.com/$query'
		fi;;
	Wiktionary)
		query=$(rofi -dmenu -i -p 'Wiktionary:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://en.wiktionary.org/
		else
			eval 'brave https://en.wiktionary.org/wiki/Special:Search?search=$query&go=Look+up&ns0=1'
		fi;;
	Merriam-Webster)
		query=$(rofi -dmenu -i -p 'Merriam-Webster:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://www.merriam-webster.com/
		else
			eval 'brave https://www.merriam-webster.com/dictionary/$query'
		fi;;
	Urban-Dictionary)
		query=$(rofi -dmenu -i -p 'Urban-Dictionary:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://www.urbandictionary.com/
		else
			eval 'brave https://www.urbandictionary.com/define.php?term=$query'
		fi;;
	Oxford-Collocation-Dictionary)
		query=$(rofi -dmenu -i -p 'Oxford-Collocation-Dictionary:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://www.freecollocation.com/
		else
			eval 'brave https://www.freecollocation.com/search?word=$query'
		fi;;
	Abbreviations)
		query=$(rofi -dmenu -i -p 'Abbreviations:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://www.abbreviations.com/
		else
			eval 'brave https://www.abbreviations.com/$query'
		fi;;
	Odysee)
		query=$(rofi -dmenu -i -p 'Odysee:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://odysee.com/
		else
			eval 'brave https://odysee.com/$/search?q=$query'
		fi;;
	BitChute)
		query=$(rofi -dmenu -i -p 'BitChute:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://www.bitchute.com/
		else
			eval 'brave https://www.bitchute.com/search/?query=$query'
		fi;;
	Rumble)
		query=$(rofi -dmenu -i -p 'Rumble:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://rumble.com/
		else
			eval 'brave https://rumble.com/search/video?q=$query'
		fi;;
	Invidious)
		query=$(rofi -dmenu -i -p 'Invidious:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://yewtu.be/
		else
			eval 'brave https://yewtu.be/search?q=$query'
		fi;;
	YouTube)
		query=$(rofi -dmenu -i -p 'YouTube:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://www.youtube.com/
		else
			eval 'brave https://www.youtube.com/results?search_query=$query'
		fi;;
	DevTube)
		query=$(rofi -dmenu -i -p 'DevTube:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://dev.tube/
		else
			eval 'brave https://dev.tube/?q=$query'
		fi;;
	Minds)
		query=$(rofi -dmenu -i -p 'Minds:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://www.minds.com/
		else
			eval 'brave https://www.minds.com/discovery/search?q=$query&f=top&t=all'
		fi;;
	AlternativeTo)
		query=$(rofi -dmenu -i -p 'AlternativeTo:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://alternativeto.net/
		else
			eval 'brave https://alternativeto.net/browse/search?q=$query'
		fi;;
	Arch-Wiki)
		query=$(rofi -dmenu -i -p 'Arch-Wiki:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://wiki.archlinux.org/
		else
			eval 'brave https://wiki.archlinux.org/index.php?search=$query'
		fi;;
	Arch-Packages)
		query=$(rofi -dmenu -i -p 'Arch-Packages:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://archlinux.org/
		else
			eval 'brave https://archlinux.org/packages/?q=$query'
		fi;;
	AUR-Packages)
		query=$(rofi -dmenu -i -p 'AUR-Packages:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://aur.archlinux.org/
		else
			eval 'brave https://aur.archlinux.org/packages/?K=$query'
		fi;;
	PKGS)
		query=$(rofi -dmenu -i -p 'PKGS:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://pkgs.org/
		else
			eval 'brave https://pkgs.org/search/?q=$query'
		fi;;
	Kupujem-Prodajem)
		query=$(rofi -dmenu -i -p 'Kupujem-Prodajem:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://novi.kupujemprodajem.com/
		else
			eval 'brave https://novi.kupujemprodajem.com/pretraga?keywords=$query'
		fi;;
	Instant-Gaming)
		query=$(rofi -dmenu -i -p 'Instant-Gaming:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://www.instant-gaming.com/
		else
			eval 'brave https://www.instant-gaming.com/en/search/?q=$query'
		fi;;
	ProtonDB)
		query=$(rofi -dmenu -i -p 'ProtonDB:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://www.protondb.com/
		else
			eval 'brave https://www.protondb.com/search?q=$query'
		fi;;
	IMDB)
		query=$(rofi -dmenu -i -p 'IMDB:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://www.imdb.com/
		else
			eval 'brave https://www.imdb.com/find?q=$query'
		fi;;
	GitLab)
		query=$(rofi -dmenu -i -p 'GitLab:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://gitlab.com/
		else
			eval 'brave https://gitlab.com/search?search=$query'
		fi;;
	GitHub)
		query=$(rofi -dmenu -i -p 'GitHub:' -no-fixed-num-lines -no-steal-focus -theme /home/radagah/.config/archish-gah/rofi-themes/ag-list | sed 's/ /+/g')
		if [[ $query == '' ]]; then
			brave https://github.com/
		else
			eval 'brave https://github.com/search?q=$query'
		fi;;
esac
