#!/bin/bash

calendar_theme="$HOME/.config/archish-gah/rofi-themes/ag-calendar"
session=$(set | grep DESKTOP_SESSION | sed 's/DESKTOP_SESSION=//g')

full_date=$(date +%F)
day_name_short=$(date +%a)
day_name_long=$(date +%A)
day_num=$(date +%d)
month_name_short=$(date +%b)
month_name_long=$(date +%B)
month_num=$(date +%m)
year=$(date +%Y)
cal_m=$(cal -m)

cal_string="$(echo -e "$cal_m" | awk 'NR > 1{print t} {t = $0}END{if (NF) print }' | tail -n +2)"
calendar_text=" $day_name_short, $day_num $month_name_short, $year.\n$cal_string"
calendar_lines=$(echo -e "$calendar_text" | wc -l)

echo -e "$calendar_text" | awk 'NR > 1 { print t } { t = $0 } END { if (NF) print }' | rofi -dmenu -theme $calendar_theme -l $calendar_lines