#!/bin/bash

xdotool behave_screen_edge --delay 0 top windowstate --add MAXIMIZED_HORZ $(xdotool getactivewindow) windowstate --add MAXIMIZED_VERT $(xdotool getactivewindow)