#!/bin/bash

current_user=$HOME

function copy_icons() {
	papirus_ag_dest="/usr/share/icons/Papirus-AG/"
	
	papirus_ag_source="$current_user/.config/archish-gah/icons/Papirus-AG-source/"
	main_folder=$(ls -Ah "$papirus_ag_source")
	
	for folder in $main_folder; do
		if [[ "$folder" != "index.theme" ]]; then
			subfolders=$(ls -Ah "$papirus_ag_source$folder" | sed 's/ /\n/g')
			for subfolder in $subfolders; do
				icons=$(ls -Ah "$papirus_ag_source$folder/$subfolder" | sed 's/ /\n/g')
				for icon in $icons; do
					if [[ ! -d "$papirus_ag_dest$folder/$subfolder" ]]; then
						mkdir -p "$papirus_ag_dest$folder/$subfolder"
					fi
					cp "$papirus_ag_source$folder/$subfolder/$icon" "$papirus_ag_dest$folder/$subfolder/$icon"
				done
			done
		fi
	done
}

pkexec copy_icons