#!/bin/bash

function run {
	if ! pgrep -f $1 ; then
		$@&
	fi
}

pidof pasystray | xargs kill -9
run dex -a -e Awesome
run gxkb
run numlockx
run nitrogen --restore
run picom -b --config ~/.config/archish-gah/picom.conf