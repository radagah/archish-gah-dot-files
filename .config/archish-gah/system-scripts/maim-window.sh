#!/bin/bash

img_name="window_$(date +%Y-%m-%d-%T).png"
icon="/usr/share/icons/Papirus-AG/64x64/mimetypes/image-x-generic.svg"

maim -u -i $(xdotool getactivewindow) | tee ~/Pictures/$img_name | xclip -selection clipboard -t image/png

notify-send.py -u low -t 2300 -i $icon "Screenshot stored in:\n$HOME/Pictures/$img_name"