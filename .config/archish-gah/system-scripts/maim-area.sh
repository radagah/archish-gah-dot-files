#!/bin/bash

img_name="area_$(date +%Y-%m-%d-%T).png"
icon="/usr/share/icons/Papirus-AG/64x64/mimetypes/image-x-generic.svg"

maim -s -u | tee ~/Pictures/$img_name | xclip -selection clipboard -t image/png

if [[ -f ~/Pictures/$img_name ]]; then
	notify-send.py -u low -t 2300 -i $icon "Screenshot stored in:\n$HOME/Pictures/$img_name"
fi