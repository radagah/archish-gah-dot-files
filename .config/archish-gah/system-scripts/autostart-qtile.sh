#!/bin/bash

function run {
	if ! pgrep -f $1 ; then
		$@&
	fi
}

run dunst
run dex -a -e Qtile
run numlockx
run nitrogen --restore
run picom -b --config ~/.config/archish-gah/picom.conf