#!/bin/bash

x_mouse_location=$(xdotool getmouselocation | awk '{print $1}' | sed 's/x://g')
sleep_time=0.0001

if [[ $x_mouse_location -gt 1920 ]]; then
	xdotool mousemove 960 30
	sleep $sleep_time
	xdotool click 1
else
	xdotool mousemove 2880 30
	sleep $sleep_time
	xdotool click 1
fi