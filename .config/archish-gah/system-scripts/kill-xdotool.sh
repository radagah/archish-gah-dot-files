#!/bin/bash

MOUSE_ID=$(xinput --list | grep -i -m 1 'mouse' | grep -o 'id=[0-9]\+' | grep -o '[0-9]\+')

STATE1=$(xinput --query-state $MOUSE_ID | grep 'button\[1\]' | sed 's/\t//g;s/button\[1\]=//g')

while true; do
	sleep 0.1
    STATE2=$(xinput --query-state $MOUSE_ID | grep 'button\[1\]' | sed 's/\t//g;s/button\[1\]=//g')
    statement=$(comm -13 <(echo "$STATE1") <(echo "$STATE2"))
    if [[ "$statement" == "up" ]]; then
		pkill -f xdotool
	fi
    STATE1=$STATE2
done