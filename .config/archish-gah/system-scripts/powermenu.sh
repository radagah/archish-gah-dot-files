#!/bin/bash

suspend="Suspend"
hibernate="Hibernate"
shutdown="Shut Down"
reboot="Reboot"
logout="Log Out"
lock="Lock"

selection="$suspend\n$hibernate\n$shutdown\n$reboot\n$logout\n$lock"

#uptime=$(uptime -p | sed -e 's/up //g')
#date=$(date +%A", "%B" "%d", "%Y" - "%H:%M)

dir="$HOME/.config/archish-gah/rofi-themes/"
powermenu_theme="ag-powermenu"
confirmation_theme="ag-confirmation"

rofi_powermenu="rofi -i -dmenu -l 6 -selected-row 5 -show-icons -theme $dir$powermenu_theme"
rofi_confirmation="rofi -i -dmenu -selected-row 1 -no-click-to-exit -theme $dir$confirmation_theme"


session=$(set | grep DESKTOP_SESSION | sed 's/DESKTOP_SESSION=//g')

function confirmation() {
		yes="Yes"
		no="No"

		yes_no="$yes\n$no"

		choice=$(echo -e $yes_no | $rofi_confirmation)

		case $choice in
			$yes) echo $yes;;
			$no) echo $no;;
		esac
	}

chosen="$(echo -en "$selection" | $rofi_powermenu)"

case $chosen in
    $suspend)
		ans=$(confirmation &)
		if [[ $ans == "Yes" ]]; then
			systemctl suspend
		elif [[ $ans == "No" ]]; then
			exit 0
        fi
        ;;
    $hibernate)
		ans=$(confirmation &)
		if [[ $ans == "Yes" ]]; then
			systemctl hibernate
		elif [[ $ans == "No" ]]; then
			exit 0
        fi
        ;;
    $shutdown)
		ans=$(confirmation &)
		if [[ $ans == "Yes" ]]; then
			systemctl poweroff
		elif [[ $ans == "No" ]]; then
			exit 0
        fi
        ;;
    $reboot)
		ans=$(confirmation &)
		if [[ $ans == "Yes" ]]; then
			systemctl reboot
		elif [[ $ans == "No" ]]; then
			exit 0
        fi
        ;;
    $logout)
		ans=$(confirmation &)
		if [[ $ans == "Yes" ]]; then
			if [[ $session == "qtile" ]]; then
				pkill $session
			elif [[ $session == "awesome" ]]; then
				pkill $session
			elif [[ $session == "openbox" ]]; then
				$session --exit
			fi
		elif [[ $ans == "No" ]]; then
			exit 0
        else
			msg
        fi
        ;;
	$lock)
        ans=$(confirmation &)
		if [[ $ans == "Yes" ]]; then
			light-locker-command -l
		elif [[ $ans == "No" ]]; then
			exit 0
        fi
        ;;
esac