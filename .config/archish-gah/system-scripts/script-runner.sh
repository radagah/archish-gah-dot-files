#!/bin/bash

rofi_theme="$HOME/.config/archish-gah/rofi-themes/ag-list"
app_scripts="$HOME/.config/archish-gah/app-scripts/"
scripts="$(ls $app_scripts | sed 's/ /\n/g')"
line_num=$(echo -e "$scripts" | wc -l)
prompt="Choose Script:"
message="Script directory: $app_scripts"

choose=$(echo -e "$scripts" | rofi -dmenu -i -p "$prompt" -mesg "$message" -l $line_num -theme $rofi_theme)

sh "$app_scripts$choose"