#!/bin/bash

pactl set-sink-volume $(pacmd list-sinks | grep '* index' | cut -b12-) +2%

active_sink=$(pacmd list-sinks | grep '* index' | cut -b12-)

volume=$(pactl get-sink-volume $active_sink | awk '{ print $5 }' | sed 's/%//g')

message="          Volume:          $volume%          "
notification_id="$HOME/.config/archish-gah/conf-files/notification-id"
icon="$HOME/.config/archish-gah/icons/custom/volume-up.svg"

if [[ -e $notification_id ]]; then
	file_time="$(ls --full-time $(echo $notification_id) | awk '{ print $6,$7 }' | awk -F "." '{ print $1 }')"
else
	touch $notification_id
	file_time="0000-00-00 00:00:00"
fi

function check_time() {
		if [[ $1 == $3 ]]; then
			if [[ $(echo $2 | awk -F ":" '{ print $1 }') == $( echo $4 | awk -F ":" '{ print $1 }') ]]; then
				if [[ $(echo $2 | awk -F ":" '{ print $2 }') == $(echo $4 | awk -F ":" '{ print $2 }') ]]; then
					seconds_ft=$(echo $2 | awk -F ":" '{ print $3 }')
					seconds_ct=$(echo $4 | awk -F ":" '{ print $3 }')
					difference=$(( $seconds_ct-$seconds_ft ))
					if [[ $difference -le 5 ]]; then
						nid=$(cat $notification_id)
						nid=$(notify-send.py -u low "$message" -r $nid -i $icon -t 2300)
						echo $nid > $notification_id
					else
						nid=0
						nid=$(notify-send.py -u low "$message" -r $nid -i $icon -t 2300)
						echo $nid > $notification_id
					fi
				else
					nid=0
					nid=$(notify-send.py -u low "$message" -r $nid -i $icon -t 2300)
					echo $nid > $notification_id
				fi
			else
				nid=0
				nid=$(notify-send.py -u low "$message" -r $nid -i $icon -t 2300)
				echo $nid > $notification_id
			fi
		else
			nid=0
			nid=$(notify-send.py -u low "$message" -r $nid -i $icon -t 2300)
			echo $nid > $notification_id
		fi
	}

check_time $file_time $(date "+%Y-%m-%d %T")