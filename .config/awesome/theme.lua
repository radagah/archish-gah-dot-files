----------------------------------
-- "Archish Gah" awesome theme	--
-- By Alexei Radagah (radagahalexei)	--
----------------------------------


local home_dir		= os.getenv ("HOME")
local conf_dir		= require("gears.filesystem").get_xdg_config_home ()
local ag_dir		= conf_dir .. "/archish-gah"
local icon_dir		= ag_dir .. "/icons"
local papirus_dir	= "/usr/share/icons/Papirus-AG"
local dpi			= require("beautiful.xresources").apply_dpi
local theme 		= {}


--[[ Font and systray settings. ]]
theme.font					= "Jetbrains Mono 12"
theme.icon_theme			= "Papirus-AG"
theme.systray_icon_spacing	= dpi(5)


--[[ Titlebar theme. ]]
theme.fg_normal		= "#EEEEEE"
theme.fg_focus		= "#EEEEEE"
theme.fg_urgent		= "#FF4D4D"
theme.bg_normal		= "#2E2E2E"
theme.bg_focus		= "#494949"
theme.bg_urgent		= "#2E2E2E"
theme.bg_systray	= theme.bg_normal


--[[ Borders and gaps. ]]
theme.useless_gap	= dpi(2)
theme.border_width	= dpi(2)
theme.border_color	= "#5294E2"
theme.border_normal	= "#6F6F6F"
theme.border_focus	= "#5294E2"
theme.border_marked	= "#FF4D4D"


-- There are other variable sets overriding the default one when defined, the sets are:
-- [taglist|tasklist]_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- titlebar_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- Example:
--theme.taglist_bg_focus = "#6F6F6F"


--[[ Titlebar theme. ]]
theme.titlebar_bg_focus		= "#2E2E2E"
theme.titlebar_bg_normal	= "#3E3E3E"


--[[ Taglist theme. ]]
theme.taglist_bg_empty		= "#2E2E2E"
theme.taglist_bg_occupied	= "#494949"
theme.taglist_bg_focus		= "#6F6F6F"
theme.taglist_fg_normal		= "#AAAAAA"
theme.taglist_fg_focus		= "#EEEEEE"
theme.taglist_fg_urgent		= "#FF4D4D"
theme.taglist_fg_empty		= "#AAAAAA"
theme.taglist_fg_occupied	= "#EEEEEE"

--[[ Tasklist theme. ]]
theme.tasklist_fg_normal	= "#AAAAAA"
theme.tasklist_fg_focus		= "#EEEEEE"
theme.tasklist_fg_urgent	= "#FF4D4D"


-- You can add as many variables as you wish and access them by using beautiful.variable in your rc.lua
--theme.fg_widget        = "#AECF96"
--theme.fg_center_widget = "#88A175"
--theme.fg_end_widget    = "#FF5656"
--theme.bg_widget        = "#494B4F"
--theme.border_widget    = "#3F3F3F"


theme.mouse_finder_color	= "#CC9393"
--mouse_finder_[timeout|animate_timeout|radius|factor]


-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_border_width		= dpi(2)
theme.menu_border_color		= "#5294E2"
theme.menu_height			= dpi(32)
theme.menu_width			= dpi(270)


--theme.taglist_squares_resize		= "false"
theme.taglist_squares_sel			= icon_dir .. "/bar-decorations/line_active.svg"
theme.taglist_squares_unsel			= icon_dir .. "/bar-decorations/line_inactive.svg"

theme.tasklist_bg_image_normal		= icon_dir .. "/bar-decorations/line_inactive.svg"
theme.tasklist_bg_image_focus		= icon_dir .. "/bar-decorations/line_active.svg"
theme.tasklist_bg_image_urgent		= icon_dir .. "/bar-decorations/line_urgent.svg"
theme.tasklist_bg_image_minimize	= icon_dir .. "/bar-decorations/line_inactive.svg"


--[[ Hotkeys theme. ]]
theme.hotkeys_bg					= "#2E2E2E"
theme.hotkeys_fg					= "#EEEEEE"
theme.hotkeys_modifiers_fg			= "#5294E2"
theme.hotkeys_label_bg				= "#FFFFFF"
theme.hotkeys_label_fg				= "#000000"
theme.hotkeys_border_width			= dpi(2)
theme.hotkeys_group_margin			= dpi(7)
theme.hotkeys_border_color			= "#5294E2"
theme.hotkeys_font					= "Jetbrains Mono 9"
theme.hotkeys_description_font		= "Jetbrains Mono 8"


--[[ Notification theme. ]]
-- notification_font
-- notification_[bg|fg]
-- notification_[width|height|margin]
-- notification_[border_color|border_width|shape|opacity]
theme.notification_border_color		= "#5294E2"
theme.notification_icon_size		= dpi(48)


--[[ Icon locations. ]]
theme.archish_gah								= icon_dir		.. "/custom/archish-gah.svg"
theme.menu_submenu_icon							= icon_dir		.. "/custom/submenu.svg"
theme.powermenu									= icon_dir		.. "/custom/exit.svg"
theme.awesome									= icon_dir		.. "/custom/awesome.svg"
theme.awesome_manual							= icon_dir		.. "/custom/awesome-manual.svg"
theme.awesome_edit_conf							= icon_dir		.. "/custom/awesome-edit-conf.svg"
theme.awesome_restart							= icon_dir		.. "/custom/awesome-restart.svg"
theme.streaming									= icon_dir		.. "/custom/streaming.svg"
theme.brave										= papirus_dir	.. "/64x64/apps/brave-browser.svg"
theme.brave_incognito							= papirus_dir	.. "/64x64/apps/brave-browser-incognito.svg"
theme.brave_tor									= papirus_dir	.. "/64x64/apps/brave-browser-tor.svg"
theme.alacritty									= papirus_dir	.. "/64x64/apps/Alacritty.svg"
theme.system_monitor							= papirus_dir	.. "/64x64/apps/utilities-system-monitor.svg"
theme.file_manager								= papirus_dir	.. "/64x64/apps/system-file-manager.svg"
theme.change_wallpaper							= papirus_dir	.. "/64x64/apps/preferences-desktop-wallpaper.svg"
theme.change_theme								= papirus_dir	.. "/64x64/apps/preferences-desktop-theme.svg"
theme.login_manager								= papirus_dir	.. "/64x64/apps/lightdm-gtk-greeter-settings.svg"
theme.suspend									= papirus_dir	.. "/64x64/apps/system-suspend.svg"
theme.hibernate									= papirus_dir	.. "/64x64/apps/system-suspend-hibernate.svg"
theme.shutdown									= papirus_dir	.. "/64x64/apps/system-shutdown.svg"
theme.reboot									= papirus_dir	.. "/64x64/apps/system-reboot.svg"
theme.logout									= papirus_dir	.. "/64x64/apps/system-log-out.svg"
theme.lock										= papirus_dir	.. "/64x64/apps/system-lock-screen.svg"
theme.keybindings								= papirus_dir	.. "/64x64/devices/input-keyboard.svg"


--[[ Layout icons. ]]
theme.layout_max								= icon_dir .. "/layouts/max.svg"
theme.layout_tile								= icon_dir .. "/layouts/tile.svg"
theme.layout_tileleft							= icon_dir .. "/layouts/tileleft.svg"
theme.layout_tilebottom							= icon_dir .. "/layouts/tilebottom.svg"
theme.layout_tiletop							= icon_dir .. "/layouts/tiletop.svg"
theme.layout_fairv								= icon_dir .. "/layouts/fairv.svg"
theme.layout_fairh								= icon_dir .. "/layouts/fairh.svg"
theme.layout_spiral								= icon_dir .. "/layouts/spiral.svg"
theme.layout_dwindle							= icon_dir .. "/layouts/dwindle.svg"
theme.layout_fullscreen							= icon_dir .. "/layouts/fullscreen.svg"
theme.layout_magnifier							= icon_dir .. "/layouts/magnifier.svg"
theme.layout_floating							= icon_dir .. "/layouts/floating.svg"
theme.layout_cornernw							= icon_dir .. "/layouts/cornernw.svg"
theme.layout_cornerne							= icon_dir .. "/layouts/cornerne.svg"
theme.layout_cornersw							= icon_dir .. "/layouts/cornersw.svg"
theme.layout_cornerse							= icon_dir .. "/layouts/cornerse.svg"


--[[ Titlebar icons. ]]
theme.titlebar_close_button_focus				= icon_dir .. "/titlebar/close_focus.svg"
theme.titlebar_close_button_normal				= icon_dir .. "/titlebar/close_normal.svg"

theme.titlebar_maximized_button_focus_active	= icon_dir .. "/titlebar/maximized_focus_active.svg"
theme.titlebar_maximized_button_normal_active	= icon_dir .. "/titlebar/maximized_normal_active.svg"
theme.titlebar_maximized_button_focus_inactive	= icon_dir .. "/titlebar/maximized_focus_inactive.svg"
theme.titlebar_maximized_button_normal_inactive	= icon_dir .. "/titlebar/maximized_normal_inactive.svg"

theme.titlebar_minimize_button_normal			= icon_dir .. "/titlebar/minimize_normal.svg"
theme.titlebar_minimize_button_focus			= icon_dir .. "/titlebar/minimize_focus.svg"

theme.titlebar_ontop_button_focus_active		= icon_dir .. "/titlebar/ontop_focus_active.svg"
theme.titlebar_ontop_button_normal_active		= icon_dir .. "/titlebar/ontop_normal_active.svg"
theme.titlebar_ontop_button_focus_inactive		= icon_dir .. "/titlebar/ontop_focus_inactive.svg"
theme.titlebar_ontop_button_normal_inactive		= icon_dir .. "/titlebar/ontop_normal_inactive.svg"

theme.titlebar_sticky_button_focus_active		= icon_dir .. "/titlebar/sticky_focus_active.svg"
theme.titlebar_sticky_button_normal_active		= icon_dir .. "/titlebar/sticky_normal_active.svg"
theme.titlebar_sticky_button_focus_inactive  	= icon_dir .. "/titlebar/sticky_focus_inactive.svg"
theme.titlebar_sticky_button_normal_inactive 	= icon_dir .. "/titlebar/sticky_normal_inactive.svg"

theme.titlebar_floating_button_focus_active		= icon_dir .. "/titlebar/floating_focus_active.svg"
theme.titlebar_floating_button_normal_active	= icon_dir .. "/titlebar/floating_normal_active.svg"
theme.titlebar_floating_button_focus_inactive	= icon_dir .. "/titlebar/floating_focus_inactive.svg"
theme.titlebar_floating_button_normal_inactive	= icon_dir .. "/titlebar/floating_normal_inactive.svg"


return theme