--[[ If LuaRocks is installed, make sure that packages installed through it are
found (e.g. lgi). If LuaRocks is not installed, do nothing. ]]
pcall(require, "luarocks.loader")

--[[ Standard awesome library. ]]
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")

--[[ Widget and layout library. ]]
local wibox		= require("wibox")
local vicious	= require("vicious")

--[[ Theme handling library. ]]
local beautiful = require("beautiful")
local dpi		= require("beautiful.xresources").apply_dpi

--[[ Notification library. ]]
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")

--[[ Enable hotkeys help widget for VIM and other apps when client with a matching name is opened. ]]
--require("awful.hotkeys_popup.keys")

--[[ Error handling. ]]
if awesome.startup_errors then
	naughty.notify(
		{
			preset = naughty.config.presets.critical,
			title = "Oops, there were errors during startup!",
			text = awesome.startup_errors
		})
end
do
	local in_error = false
	awesome.connect_signal("debug::error",
		function (err)
			if in_error then return end
			in_error = true
			naughty.notify(
				{
					preset	= naughty.config.presets.critical,
					title	= "Oops, an error happened!",
					text	= tostring(err)
				})
			in_error = false
		end)
end

--[[ Directory path variables. ]]
home_dir			= os.getenv ("HOME")
conf_dir			= gears.filesystem.get_xdg_config_home ()
ag_dir				= conf_dir .. "/archish-gah"
awesome_dir			= conf_dir .. "/awesome"
icon_dir			= ag_dir .. "/icons"
rofi_themes			= ag_dir .. "/rofi-themes"
app_scripts			= ag_dir .. "/app-scripts"
system_scripts		= ag_dir .. "/system-scripts"

--[[ Command and abbreviation variables. ]]
terminal					= "alacritty"
system_monitor				= terminal .. " --class 'System Monitor','System Monitor' -t 'BTop++' -e btop"
browser						= "brave"
browser_incognito			= "brave --incognito"
browser_tor					= "brave --tor"
streaming					= "brave --incognito https://www.odysee.com https://www.rumble.com https://www.bitchute.com/ https://www.youtube.com"
awesome_manual				= browser .. " --app=https://awesomewm.org/doc/manpages/awesome.1.html"
file_manager				= "pcmanfm"
change_wallpaper			= "nitrogen"
change_theme				= "lxappearance"
change_lm					= "lightdm-gtk-greeter-settings-pkexec"
editor						= "geany"
editor_var					= "nano"
editor_cmd					= terminal .. " -e " .. editor_var

--[[ Different programming language shells. ]]
python_shell		= terminal .. " --class 'Python Shell','Python Shell' -t 'Python Shell' -e python -q"

--[[ Powermenu. ]]
suspend				= "systemctl suspend"
hibernate			= "systemctl hibernate"
shutdown			= "systemctl poweroff"
reboot				= "systemctl reboot"
lock				= "light-locker-command -l"

--[[ Key variables. ]]
alt					= "Mod1"
num_lock			= "Mod2"
modkey				= "Mod4"
caps_lock			= "Lock"
numpad = {
		"KP_Insert",	-- KEY PAD 0
		"KP_End",		-- KEY PAD 1
		"KP_Down",		-- KEY PAD 2
		"KP_Next",		-- KEY PAD 3
		"KP_Left",		-- KEY PAD 4
		"KP_Begin",		-- KEY PAD 5
		"KP_Right",		-- KEY PAD 6
		"KP_Home",		-- KEY PAD 7
		"KP_Up",		-- KEY PAD 8
		"KP_Prior",		-- KEY PAD 9
		}

--[[ Rofi command variables. ]]
rofi_drun			= 'rofi -show drun -theme ~/.config/archish-gah/rofi-themes/ag-drun.rasi'
rofi_window			= 'rofi -show window -theme ~/.config/archish-gah/rofi-themes/ag-window.rasi'
rofi_windowcd		= 'rofi -show windowcd -theme ~/.config/archish-gah/rofi-themes/ag-window.rasi'
rofi_powermenu		= system_scripts	.. "/powermenu.sh"
rofi_unified_search	= app_scripts		.. "/unified-search.sh"
rofi_script_runner	= system_scripts	.. "/script-runner.sh"
rofi_calendar		= system_scripts	.. "/calendar.sh"

--[[ Theming. ]]
beautiful.init(awesome_dir .. "/theme.lua")


--[[ Notifications theming. ]]
naughty.config.padding						= dpi(13)
naughty.config.defaults.border_width		= dpi(2)
naughty.config.defaults.margin				= dpi(13)


--[[ Table of layouts to cover with awful.layout.inc, order matters. ]]
als	= awful.layout.suit

awful.layout.layouts = {
	als.max,
	als.tile.left,
	--als.tile.right,
	--als.tile.top,
	--als.tile.bottom,
	--als.floating,
	--als.max.fullscreen,
	--als.spiral,
	--als.spiral.dwindle,
	--als.corner.nw,
	--als.corner.ne,
	--als.corner.sw,
	--als.corner.se,
	--als.magnifier,
}

--[[ Create a launcher widget and a main menu. ]]
agmenu = {
	{ "Keybindings",		function () hotkeys_popup.show_help(nil, awful.screen.focused()) end,	beautiful.keybindings },
	{ "System Monitor",		system_monitor,															beautiful.system_monitor },
	{ "Change Wallpaper",	change_wallpaper,														beautiful.change_wallpaper },
	{ "Change Theme",		change_theme,															beautiful.change_theme },
	{ "Edit Login Manager",	change_lm,																beautiful.login_manager },
}

mypowermenu = {
	{ "Suspend",			suspend,																beautiful.suspend },
	{ "Hibernate",			hibernate,																beautiful.hibernate },
	{ "Shut Down",			shutdown,																beautiful.shutdown },
	{ "Reboot",				reboot,																	beautiful.reboot },
	{ "Log Out",			function () awesome.quit() end,											beautiful.logout },
	{ "Lock",				lock,																	beautiful.lock },
}

myawesomemenu = {
	{ "Awesome Manual",		awesome_manual,															beautiful.awesome_manual },
	{ "Edit Config File",	editor .. " " .. awesome.conffile,										beautiful.awesome_edit_conf },
	{ "Edit Theme File",	editor .. " " .. awesome_dir .. "/theme.lua",							beautiful.awesome_edit_conf },
	{ "Reset Awesome",		awesome.restart,														beautiful.awesome_restart },
}

mymainmenu = awful.menu(
				{ items = {
						{ "Archish Gah",		agmenu,					beautiful.archish_gah },
						{ "Awesome Menu",		myawesomemenu,			beautiful.awesome },
						{ "Brave",				browser,				beautiful.brave },
						{ "Brave (incognito)",	browser_incognito,		beautiful.brave_incognito },
						{ "Brave (tor)",		browser_tor,			beautiful.brave_tor },
						{ "Media Streaming",	streaming,				beautiful.streaming },
						{ "File Manager",		file_manager,			beautiful.file_manager },
						{ "Terminal",			terminal,				beautiful.alacritty },
						{ "Powermenu",			mypowermenu,			beautiful.powermenu },
					}
				})

--[[ Menubar configuration. ]]
menubar.utils.terminal = terminal -- Set the terminal for applications that require it

--[[ Taglist mouse keybindings. ]]
local taglist_buttons = gears.table.join(
							awful.button(
								{ }, 1,
								function (t)
									t:view_only()
								end),
							awful.button(
								{ modkey, }, 1,
								function (t)
									if client.focus then
										client.focus:move_to_tag(t)
									end
								end),
							awful.button(
								{ }, 3,
								awful.tag.viewtoggle),
							awful.button(
								{ modkey, }, 3,
								function (t)
									if client.focus then
										client.focus:toggle_tag(t)
									end
								end),
							awful.button(
								{ }, 4,
								function (t)
									awful.tag.viewprev(t.screen)
								end),
							awful.button(
								{ }, 5,
								function (t)
									awful.tag.viewnext(t.screen)
								end)
						)


--[[ Tasklist mouse keybindings. ]]
local tasklist_buttons = gears.table.join(
					awful.button(
						{ }, 1,
						function (c)
							if c == client.focus then
								c.minimized = true
							else
								c:emit_signal(
									"request::activate",
									"tasklist",
									{ raise = true }
								)
							end
						end),
					awful.button(
						{ }, 2,
						function (c)
							c:kill()
						end),
					awful.button(
						{ }, 3,
						function ()
							awful.menu.client_list()
						end),
					awful.button(
						{ }, 4,
						function ()
							awful.client.focus.byidx(1)
						end),
					awful.button(
						{ }, 5,
						function ()
							awful.client.focus.byidx(-1)
						end)
				)

awful.screen.connect_for_each_screen(function (s)

    --[[ Each screen has its own tag table. ]]
    local names = { " 1 ", " 2 ", " 3 ", " 4 ", " 5 ", " 6 ", " 7 ", " 8 ", " 9 " }
    local layouts = {
				als.max,
				als.max,
				als.max,
				als.max,
				als.max,
				als.max,
				als.max,
				als.max,
				als.max,
			}
	awful.tag(names, s, layouts)
	
	--[[ Awesome prompt box. ]]
	s.mypromptbox = awful.widget.prompt()

	--[[ Create a textclock and calendar widget. ]]
	local mytextclock = wibox.container.margin(wibox.widget.textclock("%Y/%m/%d %I:%M:%S %p", 1), 5, 0, 2, 0)
	mytextclock:buttons(
		awful.util.table.join(
			awful.button(
				{ }, 1,
				function ()
					awful.spawn.with_shell(rofi_calendar)
				end)
		)
	)

	--[[ Create a layout widget which will contain an icon indicating which layout we're using. ]]
	s.mylayoutbox = wibox.container.margin(awful.widget.layoutbox(s), 0, 9, 2, 2)
	s.mylayoutbox:buttons(
		gears.table.join(
			awful.button(
				{ }, 1,
				function ()
					awful.layout.inc(1)
				end),
			awful.button(
				{ }, 3,
				function ()
					awful.layout.inc(-1)
				end),
			awful.button(
				{ }, 4,
				function ()
					awful.layout.inc(1)
				end),
			awful.button(
				{ }, 5,
				function ()
					awful.layout.inc(-1)
				end)
		)
	)

    --[[ Create a taglist widget. ]]
    s.mytaglist	= awful.widget.taglist {
						screen	= s,
						filter	= awful.widget.taglist.filter.all,
						buttons	= taglist_buttons
					}

	--[[ Create a tasklist widget. ]]
	s.mytasklist = awful.widget.tasklist {
		screen	= s,
		filter	= awful.widget.tasklist.filter.currenttags,
		buttons	= tasklist_buttons,
		layout	= {
			spacing				= 7,
			spacing_widget		= {
				{
					forced_width	= 1,
					shape			= gears.shape.rectangle,
					widget			= wibox.widget.separator
				},
				valign	= 'center',
				halign	= 'center',
				widget	= wibox.container.place,
			},
			layout	= wibox.layout.flex.horizontal,
		},
		widget_template = {
			{
				{
					{
						{
							id		= 'icon_role',
							widget	= wibox.widget.imagebox,
						},
						margins	= 5,
						widget	= wibox.container.margin,
					},
					{
						id		= 'text_role',
						widget	= wibox.widget.textbox,
					},
					layout	= wibox.layout.fixed.horizontal,
				},
				left	= 5,
				right	= 5,
				widget	= wibox.container.margin
			},
			id				= 'background_role',
			forced_width	= 230,
			widget			= wibox.container.background,
		},
	}

	--[[ Create a rofi drun menu launcher widget. ]]
	local rofi_drun_icon = wibox.widget {
					image  = icon_dir .. "/custom/archish-gah.svg",
					forced_height = 32,
					forced_width = 32,
					resize = true,
					widget = wibox.widget.imagebox,
				}
	local rofi_drun_widget = wibox.container.margin(rofi_drun_icon, 5, 0, 3, 3)
	rofi_drun_widget:buttons(
		awful.util.table.join(
			awful.button(
				{ }, 1,
				function ()
					awful.spawn.with_shell(rofi_drun)
				end)
		)
	)

	--[[ Create a rofi powermenu launcher widget. ]]
	local rofi_powermenu_icon = wibox.widget {
					image  = icon_dir .. "/custom/exit.svg",
					forced_height = 32,
					forced_width = 32,
					resize = true,
					widget = wibox.widget.imagebox,
				}
	local rofi_powermenu_widget = wibox.container.margin(rofi_powermenu_icon, 10, 0, 3, 3)
	rofi_powermenu_widget:buttons(
		awful.util.table.join(
			awful.button(
				{ }, 1,
				function ()
					awful.spawn.with_shell(rofi_powermenu)
				end)
		)
	)

	--[[ Create the systray. ]]
	local systray_widget = wibox.container.margin(wibox.widget.systray(), 9, 5, 3, 0)

    --[[ Create the wibox. ]]
    s.mywibox = awful.wibar(
					{ position = "top", screen = s, height = 30, }
				)

    --[[ Add widgets to the wibox. ]]
	s.mywibox:setup {
		layout = wibox.layout.align.horizontal,
		{ --[[ Left widgets. ]]
			layout = wibox.layout.fixed.horizontal,
			rofi_drun_widget,
			s.mylayoutbox,
			s.mytaglist,
			s.mypromptbox,
		},
		wibox.container.margin(s.mytasklist, 9, 5, 0, 0), --[[ Middle widget. ]]
		{ --[[ Right widgets. ]]
			layout = wibox.layout.fixed.horizontal,
			systray_widget,
			mytextclock,
			rofi_powermenu_widget,
		},
	}
end)

--[[ Desktop mouse bindings. ]]
root.buttons(gears.table.join(
	awful.button(
		{ }, 3,
		function ()
			mymainmenu:toggle()
		end),
	awful.button(
		{ modkey, }, 4,
		awful.tag.viewprev),
	awful.button(
		{ modkey, }, 5,
		awful.tag.viewnext)
))

--[[ Key bindings. ]]
globalkeys = gears.table.join(
	
	--[[ DESKTOP CONTROLS ]]
	awful.key(
		{ modkey, }, "k",
		hotkeys_popup.show_help,
		{ description = "== Show the help page.", group = "01. Desktop Controls" }),
	awful.key(
		{ modkey, }, "v",
		function ()
			mymainmenu:show()
		end,
		{ description = "== Show the main menu.", group = "01. Desktop Controls" }),
	awful.key(
		{ modkey, "Control" }, "r",
		awesome.restart,
		{ description = "== Reset awesome window manager.", group = "01. Desktop Controls" }),
	awful.key(
		{ modkey, "Control" }, "q",
		awesome.quit,
		{ description = "== Quit awesome window manager.", group = "01. Desktop Controls" }),
	awful.key(
		{ alt, "Control" }, "r",
		function ()
			awful.prompt.run {
				prompt			= "Run Lua code: ",
				textbox			= awful.screen.focused().mypromptbox.widget,
				exe_callback	= awful.util.eval,
				history_path	= awful.util.get_cache_dir() .. "/history_eval"}
		end,
		{ description = "== Run the lua execute prompt.", group = "01. Desktop Controls" }),
	awful.key(
		{ modkey, }, "Return",
		function ()
			awful.spawn(terminal)
		end,
		{ description = "== Open the terminal.", group = "01. Desktop Controls" }),
	awful.key(
		{ alt, "Control" }, "Delete",
		function ()
			awful.spawn(system_monitor)
		end,
		{ description = "== Open the system monitor.", group = "01. Desktop Controls" }),
	awful.key(
		{ modkey, }, "s",
		function ()
			awful.spawn.with_shell(rofi_script_runner)
		end,
		{ description = "== Run the rofi script runner.", group = "01. Desktop Controls" }),
	awful.key(
		{ modkey, }, "space",
		function ()
			awful.spawn.with_shell(rofi_drun)
		end,
		{ description = "== Run the rofi application menu.", group = "01. Desktop Controls" }),
	awful.key(
		{ modkey, }, "p",
		function ()
			awful.spawn.with_shell(python_shell)
		end,
		{ description = "== Run a python shell in the default terminal.", group = "01. Desktop Controls" }),
	
	--[[ SYSTEM CONTROLS ]]
	awful.key(
		{ }, "Print",
		function ()
			awful.spawn.with_shell(system_scripts .. "/maim-screen.sh")
		end,
		{ description = "== Take a screenshot of the entire screen.", group = "02. System Controls" }),
	awful.key(
		{ "Control", }, "Print",
		function ()
			awful.spawn.with_shell(system_scripts .. "/maim-area.sh")
		end,
		{ description = "== Take a screenshot of the selected area.", group = "02. System Controls" }),
	awful.key(
		{ alt, }, "Print",
		function ()
			awful.spawn.with_shell(system_scripts .. "/maim-window.sh")
		end,
		{ description = "== Take a screenshot of the focused window.", group = "02. System Controls" }),
	awful.key(
		{ }, "XF86AudioRaiseVolume",
		function ()
			awful.spawn.with_shell(system_scripts .. "/vol-up.sh")
		end,
		{ description = "== Raise the volume using the keyboard special keys.", group = "02. System Controls" }),
	awful.key(
		{ }, "XF86AudioLowerVolume",
		function ()
			awful.spawn.with_shell(system_scripts .. "/vol-down.sh")
		end,
		{ description = "== Lower the volume using the keyboard special keys.", group = "02. System Controls" }),
	awful.key(
		{ }, "XF86AudioMute",
		function ()
			awful.spawn.with_shell(system_scripts .. "/vol-mute.sh")
		end,
		{ description = "== Mute/Unmute the volume using the keyboard special keys.", group = "02. System Controls" }),
	
	--[[ MANAGE WINDOWS ]]
	awful.key(
		{ modkey, }, "Tab",
		function ()
			awful.client.focus.byidx(1)
		end,
		{ description = "== Select the next window by id.", group = "03. Manage Window" }),
	
	--[[ MANIPULATE WINDOWS ]]
	awful.key(
		{ modkey, }, "u",
		awful.client.urgent.jumpto,
		{ description = "== Jump to the urgent window.", group = "04. Manipulate Window" }),
	awful.key(
		{ modkey, }, "Left",
		function ()
			awful.client.focus.global_bydirection("left")
		end,
		{ description = "== Focus the window to the left.", group = "04. Manipulate Window" }),
	awful.key(
		{ modkey, }, "Right",
		function ()
			awful.client.focus.global_bydirection("right")
		end,
		{ description = "== Focus the window to the right.", group = "04. Manipulate Window" }),
	awful.key(
		{ modkey, }, "Up",
		function ()
			awful.client.focus.global_bydirection("up")
		end,
		{ description = "== Focus the window up.", group = "04. Manipulate Window" }),
	awful.key(
		{ modkey, }, "Down",
		function ()
			awful.client.focus.global_bydirection("down")
		end,
		{ description = "== Focus the window down.", group = "04. Manipulate Window" }),
	awful.key(
		{ modkey, alt }, "Left",
		function ()
			awful.client.swap.global_bydirection("left")
		end,
		{ description = "== Swap current window with window to the left.", group = "04. Manipulate Window" }),
	awful.key(
		{ modkey, alt }, "Right",
		function ()
			awful.client.swap.global_bydirection("right")
		end,
		{ description = "== Swap current window with window to the right.", group = "04. Manipulate Window" }),
	awful.key(
		{ modkey, alt }, "Up",
		function ()
			awful.client.swap.global_bydirection("up")
		end,
		{ description = "== Swap current window with window up.", group = "04. Manipulate Window" }),
	awful.key(
		{ modkey, alt }, "Down",
		function ()
			awful.client.swap.global_bydirection("down")
		end,
		{ description = "== Swap current window with window down.", group = "04. Manipulate Window" }),
	
	--[[ CHANGE LAYOUT ]]
	awful.key(
		{ modkey, }, "grave",
		function ()
			awful.layout.inc(1)
		end,
		{ description = "== Select the next available layout.", group = "05. Change Layout" }),
	awful.key(
		{ modkey, "Control" }, "Right",
		function ()
			awful.tag.incmwfact(-0.05)
		end,
		{ description = "== Increase master window width factor.", group = "05. Change Layout" }),
	awful.key(
		{ modkey, "Control" }, "Left",
		function ()
			awful.tag.incmwfact(0.05)
		end,
		{ description = "== Decrease master window width factor.", group = "05. Change Layout" }),
	awful.key(
		{ modkey, "Control" }, "Up",
		function ()
			awful.tag.incnmaster(1, nil, true)
		end,
		{ description = "== Increase the number of master windows.", group = "05. Change Layout" }),
	awful.key(
		{ modkey, "Control" }, "Down",
		function ()
			awful.tag.incnmaster(-1, nil, true)
		end,
		{ description = "== Decrease the number of master windows.", group = "05. Change Layout" }),
	awful.key(
		{ modkey, "Shift" }, "Up",
		function ()
			awful.tag.incncol(1, nil, true)
		end,
		{ description = "== Increase the number of columns.", group = "05. Change Layout" }),
	awful.key(
		{ modkey, "Shift" }, "Down",
		function ()
			awful.tag.incncol(-1, nil, true)
		end,
		{ description = "== Decrease the number of columns.", group = "05. Change Layout" }),
	
	--[[ CHANGE WORKSPACE/SCREEN ]]
	awful.key(
		{ alt, "Control" }, "Left",
		awful.tag.viewprev,
		{ description = "== View the previous workspace.", group = "06. Change Workspace/Screen" }),
	awful.key(
		{ alt, "Control" }, "Right",
		awful.tag.viewnext,
		{ description = "== View the next workspace.", group = "06. Change Workspace/Screen" }),
	awful.key(
		{ modkey, }, "BackSpace",
		awful.tag.history.restore,
		{ description = "== Go to the last used workspace.", group = "06. Change Workspace/Screen" }),
	awful.key(
		{ modkey, }, "c",
		function ()
			awful.screen.focus_relative(1)
		end,
		{ description = "== Focus the next screen.", group = "06. Change Workspace/Screen" }),
	awful.key(
		{ modkey, }, "x",
		function ()
			awful.screen.focus_relative(-1)
		end,
		{ description = "== Focus the previous screen.", group = "06. Change Workspace/Screen" })
)

clientkeys = gears.table.join(
	awful.key(
		{ modkey, }, "z",
		function ()
			awful.menu.client_list()
		end,
		{ description = "== Show a list of opened windows.", group = "01. Desktop Controls" }),
	awful.key(
		{ modkey, }, "q",
		function (c)
			c:kill()
		end,
		{ description = "== Close the focused window.", group = "03. Manage Window" }),
	awful.key(
		{ modkey, }, "w",
		function (c)
			c.maximized = not c.maximized
			c:raise()
		end,
		{ description = "== Maximize/Unmaximize window.", group = "03. Manage Window" }),
	awful.key(
		{ modkey, }, "e",
		function ()
			local c = awful.client.restore()
			if c then
				c:emit_signal("request::activate", "key.unminimize", { raise = true })
			end
		end,
		{ description = "== Restore a minimized window, unless all windows are minimized.", group = "03. Manage Window" }),
	awful.key(
		{ modkey, }, "f",
		function (c)
			if c.floating == true then
				c.width = 1908
				c.height = 1038
				c.y = 34
				if (c.x > 1520 and c.x < 3440) then
					c.x = 1924
				elseif (c.x > 3440) then
					c.x = 3844
				else
					c.x = 4
				end
			end
		end,
		awful.client.floating.toggle,
		{ description = "== Toggle window floating.", group = "03. Manage Window" }),
	awful.key(
		{ modkey, }, "a",
		function (c)
			c:swap(awful.client.getmaster())
		end,
		{ description = "== Move to master window position.", group = "03. Manage Window" }),
	awful.key(
		{ modkey, }, "d",
		function (c)
			c.minimized = true
		end,
		{ description = "== Minimize the focused window.", group = "03. Manage Window" }),
	awful.key(
		{ modkey, }, "F9",
		awful.titlebar.toggle,
		{ description = "== Toggle titlebar visibility.", group = "03. Manage Window" }),
	awful.key(
		{ modkey, }, "F10",
		function (c)
			c.ontop = not c.ontop
		end,
		{ description = "== Toggle the current window to stay on top.", group = "03. Manage Window" }),
	awful.key(
		{ modkey, }, "F11",
		function (c)
			c.fullscreen = not c.fullscreen
			c:raise()
		end,
		{ description = "== Toggle window fullscreen.", group = "03. Manage Window" }),
	awful.key(
		{ modkey, }, "F12",
		function (c)
			c.sticky = not c.sticky
		end,
		{ description = "== Toggle the current window to be sticky.", group = "03. Manage Window" }),
	awful.key(
		{ modkey, "Shift" }, "Right",
		function (c)
			c:move_to_screen(c.screen.index+1)
		end,
		{ description = "== Move the window to the right screen.", group = "04. Manipulate Window" }),
	awful.key(
		{ modkey, "Shift" }, "Left",
		function (c)
			c:move_to_screen(c.screen.index-1)
		end,
		{ description = "== Move the window to the left screen.", group = "04. Manipulate Window" })
)

--[[ Bind all key numbers to tags. ]]

for i = 1, 9 do
	globalkeys = gears.table.join(globalkeys,

		--[[ View tag only. ]]
		awful.key(
			{ modkey, }, "#" .. i + 9,
			function ()
				local screen = awful.screen.focused()
				local tag = screen.tags[i]
				if tag then
					tag:view_only()
				end
			end,
			{ description = "== View tag " .. i .. ".", group = "07. View Workspace" }),
		awful.key(
			{ modkey, }, numpad[i+1],
			function ()
				local screen = awful.screen.focused()
				local tag = screen.tags[i]
				if tag then
					tag:view_only()
				end
			end,
			{ description = "== View tag " .. i .. ".", group = "07. View Workspace" }),
		
		--[[ Move window to tag. ]]
		awful.key(
			{ modkey, "Shift" }, "#" .. i + 9,
			function ()
				if client.focus then
					local tag = client.focus.screen.tags[i]
					if tag then
						client.focus:move_to_tag(tag)
					end
				end
			end,
			{ description = "== Move focused window to tag " .. i .. ".", group = "08. Move Window to Workspace" }),
		awful.key(
			{ modkey, "Shift" }, numpad[i+1],
			function ()
				if client.focus then
					local tag = client.focus.screen.tags[i]
					if tag then
						client.focus:move_to_tag(tag)
					end
				end
			end,
			{ description = "== Move focused window to tag " .. i .. ".", group = "08. Move Window to Workspace" }),

		--[[ Toggle tag display. ]]
		awful.key(
			{ modkey, "Control" }, "#" .. i + 9,
			function ()
				local screen = awful.screen.focused()
				local tag = screen.tags[i]
				if tag then
					awful.tag.viewtoggle(tag)
				end
			end,
			{ description = "== Toggle tag " .. i .. ".", group = "09. Tag Workspace" }),
		awful.key(
			{ modkey, "Control" }, numpad[i+1],
			function ()
				local screen = awful.screen.focused()
				local tag = screen.tags[i]
				if tag then
					awful.tag.viewtoggle(tag)
				end
			end,
			{ description = "== Toggle tag " .. i .. ".", group = "09. Tag Workspace" }),

		--[[ Toggle tag on focused window. ]]
		awful.key(
			{ modkey, alt }, "#" .. i + 9,
			function ()
				if client.focus then
					local tag = client.focus.screen.tags[i]
					if tag then
						client.focus:toggle_tag(tag)
					end
				end
			end,
			{ description = "== Toggle focused window on tag " .. i .. ".", group = "10. Tag Window to Workspace" }),
		awful.key(
			{ modkey, alt }, numpad[i+1],
			function ()
				if client.focus then
					local tag = client.focus.screen.tags[i]
					if tag then
						client.focus:toggle_tag(tag)
					end
				end
			end,
			{ description = "== Toggle focused window on tag " .. i .. ".", group = "10. Tag Window to Workspace" })
		)
end

clientbuttons = gears.table.join(
    awful.button(
		{ }, 1,
		function (c)
			c:emit_signal("request::activate", "mouse_click", { raise = true })
		end),
    awful.button(
		{ modkey, }, 1,
		function (c)
			c:emit_signal("request::activate", "mouse_click", { raise = true })
			c.floating = true
			awful.mouse.client.move(c)
		end),
    awful.button(
		{ modkey, }, 3,
		function (c)
			c:emit_signal("request::activate", "mouse_click", { raise = true })
			c.floating = true
			awful.mouse.client.resize(c)
		end)
)

--[[ Set keys. ]]
root.keys(globalkeys)

--[[ Rules to apply to new clients (through the "manage" signal). ]]
awful.rules.rules = {

    --[[ All clients will match this rule. ]]
	{ rule = { },
		properties = {
			size_hints_honor		= false,	-- TO-FIX / TO-INVESTIGATE (RESIZING FLOATING DIALOGUES)
			border_width			= beautiful.border_width,
			border_color			= beautiful.border_normal,
			focus					= awful.client.focus.filter,
			raise					= true,
			keys					= clientkeys,
			buttons					= clientbuttons,
			screen					= awful.screen.preferred,
			placement				= awful.placement.no_overlap+awful.placement.no_offscreen+awful.placement.centered,
			titlebars_enabled		= false,
			maximized_vertical		= false,
			maximized_horizontal	= false,
			maximized				= false,
		},
	},
	
	--[[ Floating clients. ]]
	{ rule_any = {
		instance = {
			"DTA",  -- Firefox addon DownThemAll.
			"copyq",  -- Includes session name in class.
			"pinentry",
		},
		class = {
			"Arandr",
			"Blueman-manager",
			"Gpick",
			"Kruler",
			"MessageWin",  -- kalarm.
			"Sxiv",
			"Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
			"Wpa_gui",
			"veromix",
			"xtightvncviewer",
			"Grsync",
			"Lxappearance",
			"Qalculate-gtk",
			"Gufw.py",
			"Gsmartcontrol",
			"Pavucontrol",
			"Psensor",
			"Xarchiver",
			"Woeusbgui",
			"Usbimager",
			"Syncthing-gtk",
			"Lightdm-gtk-greeter-settings",
			"Nm-connection-editor",
			"Libfm-pref-apps",
			"Gcolor3",
			"Steam",
		},
		name = {
			"Event Tester",	-- xev.
			"Welcome to Brave" -- first time Brave launch
		},
		role = {
			"AlarmWindow",  -- Thunderbird's calendar.
			"ConfigManager",  -- Thunderbird's about:config.
			"pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
		}
	},
	except = { name = "Steam" },
	properties = { floating = true, }
	},
	{ rule = {
		class = "steam_app_*"
	},
	properties = { floating = true }
	},
	{ rule = {
		name = "Steam%s.",
	},
	properties = { floating = true }
	},
	{ rule = {
		class = "Audacium",
		name = "Audacium is starting up...",
	},
	properties = { border_width = 0 }
	},
	{ rule = {
		class = "Brave-browser"
	},
	callback = function(c) c.floating = false end
	},
}


--[[ Signal function to execute when a new client appears. ]]

client.connect_signal("manage",
	function (c)
		if not awesome.startup then
			awful.client.setslave(c)
		end
		if awesome.startup
		and not c.size_hints.user_position
		and not c.size_hints.program_position then
			awful.placement.no_offscreen(c)
		end
	end)

--[[ Add a titlebar if titlebars_enabled is set to true in the rules. ]]

client.connect_signal("request::titlebars",
	function (c)
	local buttons = gears.table.join(
		awful.button(
			{ }, 1,
			function ()
				c:emit_signal("request::activate", "titlebar", { raise = true })
				awful.mouse.client.move(c)
			end),
		awful.button(
			{ }, 3,
			function ()
				c:emit_signal("request::activate", "titlebar", { raise = true })
				awful.mouse.client.resize(c)
			end)
	)
	
	local top_titlebar = awful.titlebar(c, {
		size		= 27,
	})
	
    top_titlebar : setup {
		{ --[[ Left. ]]
			wibox.container.margin(awful.titlebar.widget.iconwidget(c), 5, 5, 5, 5),
			buttons	= buttons,
			layout	= wibox.layout.fixed.horizontal
		},
		{ --[[ Middle. ]]
			{ --[[ Title. ]]
				align	= "center",
				widget	= awful.titlebar.widget.titlewidget(c)
			},
			buttons	= buttons,
			layout	= wibox.layout.flex.horizontal
		},
		{ --[[ Right. ]]
			awful.titlebar.widget.floatingbutton (c),
			awful.titlebar.widget.stickybutton (c),
			awful.titlebar.widget.ontopbutton (c),
			awful.titlebar.widget.minimizebutton (c),
			awful.titlebar.widget.maximizedbutton (c),
			awful.titlebar.widget.closebutton (c),
			layout = wibox.layout.fixed.horizontal ()
		},
		layout = wibox.layout.align.horizontal
    }
	end
)

--[[ Changes the color of the window border when the window gets focus. ]]

client.connect_signal("focus",
	function (c)
		c.border_color = beautiful.border_focus
		c.border_width = beautiful.border_width
	end)

client.connect_signal("unfocus",
	function (c)
		c.border_color = beautiful.border_normal
		c.border_width = beautiful.border_width
	end)

client.connect_signal("manage",
	function(c)
		if c.instance ~= nil then
			local icon = menubar.utils.lookup_icon(c.instance)
			local lower_icon = menubar.utils.lookup_icon(c.instance:lower())
			if icon ~= nil then
				local new_icon = gears.surface(icon) 
				c.icon = new_icon._native
			elseif lower_icon ~= nil then 
				local new_icon = gears.surface(lower_icon)
				c.icon = new_icon._native
			elseif c.icon == nil then
				local new_icon = gears.surface(menubar.utils.lookup_icon("application-default-icon"))
				c.icon = new_icon._native
			end
		else
			local new_icon = gears.surface(menubar.utils.lookup_icon("application-default-icon"))
			c.icon = new_icon._native
		end
end)

--[[ Runs the autorun bash script at startup. ]]
awful.spawn.with_shell(system_scripts .. "/autostart-awesome.sh")