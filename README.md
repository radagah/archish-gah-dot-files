# Archish Gah
## A hack at the Awesome/Qtile/Openbox window managers.
*Primarily focused on **Awesome** and **Openbox** at the moment.*

These are dot files for Awesome, Qtile and Openbox, with some additional config files for some software I personally use.

Any suggestions or contributions are welcome, although I don't promise to incorporate them immediately or at all, depending on whether the suggested changes match up with what I have in mind for this project. I am, however, open to having my mind changed.

The name "Archish Gah" is a portmanteau-like word using Serbian and the name Arch. The translation of the Serbian pronunciation would mean something along the lines "you are hacking at it".

**P.S.** These dot files are still in their very early stages and therefore not recommended for use on mission critical machines, unless you are prepared for possible (although not experienced so far) issues.

**Dependencies:**

*(Note that these are Arch Linux packages. Adjust the names accordingly based on your Linux distribution, or whichever OS you are using.)*

*This list may not be complete and is subject to updates!*

**Arch Repository:**
- awesome-specific
	- awesome
	- dex
	- gxkb
	- vicious
- qtile-specific
	- qtile
	- dex
	- dunst
	- xcb-util-cursor
- openbox-specific
	- openbox
	- dunst
	- gxkb
	- tint2
	- xorg-xinput
- alacritty
- arandr
- btop
- geany
- geany-plugins
- light-locker
- lightdm-gtk-greeter
- lightdm-gtk-greeter-settings
- lsd
- lxappearance-gtk3
- maim
- mate-polkit
- nano
- nano-syntax-highlighting
- network-manager-applet
- nitrogen
- numlockx
- papirus-icon-theme
- pasystray
- pavucontrol
- pcmanfm-gtk3
- picom
- pulseaudio
- rofi
- ttf-jetbrains-mono
- xarchiver

**Arch User Repository**:
- brave-bin
- notify-send-py
- pikaur

**Additional packages I use**:
- audacious (music player)
- vlc (video player)
- viewnior (image viewer)
- qalculate-gtk (calculator)
- pinta (drawing application)
- libreoffice-fresh (office suite)
- qpdfview (pdf viewer)
- gufw (firewall)
- bleachbit (system cleaner)
- grsync (system backup)

**Useful tools if you plan to customize the config files:**
- wmctrl
- xdotool
- xorg-xev
- xprop